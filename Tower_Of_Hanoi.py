#towers ofhanoi in python
# Kundroo Majid
# Date: 16/10/2017

x= 1
y = 2
z = 3

def towerofhanoi(n, x, y, z):
    if n >= 1:
        towerofhanoi(n-1, x, z, y)
        print(" Move top disk from tower %s to top of tower %s " %(x, y))
        towerofhanoi(n-1, z, y, x)

n = int(input("Enter the value of n :"))
towerofhanoi(n, x, y, z)
