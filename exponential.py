# Exponential in Python
# Kundroo Majid
# Date : 12/11/2017

def exponential(x,n):
    m = n
    power = 1
    z = x
    while (m > 0):
        while(m % 2 == 0):
            z = z*z
            m = m/2
        m = m-1
        power = power * z

    print("The value of %d^%d = %d " %(x,n,power))


x = int(input("Enter the base :"))
n = int(input("Enter the exponent :"))
exponential(x,n)
