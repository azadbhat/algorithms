#selection sort in python
# Kundroo Majid
#Date : 16/10/2017

list = []
ms = 5
smallest = 0
j = 0
temp = 0

def selection_sort():
    for i in range(0, ms):
        for j in range(0, ms):
            if list[i] < list[j]:
                temp = list[i]
                list[i] = list[j]
                list[j] = temp

for i in range(0, ms):
    x = int(input("Enter the element:"))
    list.append(x)

print("The entered elements are")
print(list)
print("The sorted list is ")
selection_sort()
print (list)
