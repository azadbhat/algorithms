#binary search in python
#Kundroo Majid
#Date : 11/10/2017

list = []
def binary_search(list,n,key):
    low = 0
    hi = n - 1
    while(low <= hi):
        mid = int(hi + low /2)
        if key == list[mid]:
            return mid
        elif key < list[mid]:
            hi = mid - 1
        else:
            low = mid + 1
    return - 1
    
# function for sorting array 
def sort():
    for i in range(0,n):
        for j in range(0,n):
            if (list[i] < list[j]):
                temp = list[i]
                list[i] = list[j]
                list[j] = temp

    print("List in sorted order is ")
    print(list)

n = int(input("Enter the maximum no of elements to be stored :"))
for i in range(0,n):
    x = int(input("Enter the element :"))
    list.append(x)

print(list)
sort()
key = int(input("Enter the value to be searched :"))
pos = binary_search(list,n,key)
if(pos != -1):
    print("Entered element %d is present at position %s" %(key,pos))
else:
    print("Entered number %s is not presnt in list" %key)


