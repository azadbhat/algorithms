#Stack Implementation Using List in Python
# Kundroo Majid
# Date : 16/10/2017

print ("stack in python")
stack = []
top = -1
ms = 4
global ch
ch = 0

def push():
    global top
    if top == ms:
        print ("\n\t\tstack is full")
    else:
        top = top + 1
        print ("Enter the element to be pushed : "),
        x = int(input())
        stack.append(x)
       
def pop():
    global top
    if top == -1:
        print ("\n\t\tStack empty")
    else:
        print ("\t\tPoped element is \t%d" %stack[top])
        top = top - 1

while (ch != 3):
    print ("Select option")
    print ("1 to push")
    print ("2 to pop")
    print ("3 to exit")
    ch = int(input("Enter your choice 1 2 3:\t"))
    if ch == 1:
        push()
    elif ch == 2:
        pop()
    else:
        print ("not an option")
